import sqlite3
from db import db

# Create user object to store user data
class UserModel(db.Model):
    __tablename__ = 'Users'
    
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))


    def __init__(self, username, password):
        self.username = username
        self.password = password

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # this will use the current class
    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

        # @classmethod
        # def find_by_username(cls, username):
        # connection = sqlite3.connect('data.db')
        # cursor = connection.cursor()
        
        # query = "SELECT * FROM users WHERE username = ?"
        # #  the value should be in a tuple
        # result = cursor.execute(query, (username,))
        # row = result.fetchone()
        # if row:
        #     # instead of using User(row[0], row[1], row[2]) we use cls which represent the current class
        #     # *row it will unpack row as row[0], row[1], row[2]
        #     user = cls(*row)
        # else: 
        #     user = None

        # connection.close()
        # return user


    # this will use the current class
    # @classmethod
    # def find_by_id(cls, _id):
        # connection = sqlite3.connect('data.db')
        # cursor = connection.cursor()
        
        # query = "SELECT * FROM users WHERE id = ?"
        # #  the value should be in a tuple
        # result = cursor.execute(query, (_id,))
        # row = result.fetchone()
        # if row:
        #     # instead of using User(row[0], row[1], row[2]) we use cls which represent the current class
        #     # *row it will unpack row as row[0], row[1], row[2]
        #     user = cls(*row)
        # else: 
        #     user = None

        # connection.close()
        # return user