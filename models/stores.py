from db import db


class StoreModule(db.Model):
    __tablename__ = 'stores'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    # lazy =dynamic will no longer make items as an object in db but only query finder and to make it and object of of all items 
    # you need to use items.all()
    # however that will make the API slow as everytime there is a call it needs to access the db 
    # if you want your API to be fast delete lazy but then will make it heavy for the db
    items = db.relationship('ItemModule', lazy='dynamic')


    def __init__(self, name):
        self.name = name
    
    def json(self):
        return {'name': self.name, 'items': [item.json() for item in self.items.all()]}

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name = name).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
