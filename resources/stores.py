from flask_restful import Resource
from flask_jwt import jwt_required
from models.stores import StoreModule

class Store(Resource):
    
    def get(self, name):
        store = StoreModule.find_by_name(name)
        if store:
            return store.json()
        return {'message': "Store is not exist"}, 404
    
    def post(self, name):
            store = StoreModule.find_by_name(name)
            if store:
                return {'message': 'Store is already exists.'}, 400
            store_new = StoreModule(name)
            store_new.save_to_db()
            return store_new.json(), 201

    def delete(self, name):
        store = StoreModule.find_by_name(name)
        if store:
            store.delete()
            return {'message': 'Store deleted'}, 200
        return {'message': 'Store is not exist.'}, 400

class StoreList(Resource):
    def get(self):
        return {'stores': [store.json() for store in StoreModule.query.all()]}
