import sqlite3
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
from models.item import ItemModule

class Item(Resource):
    
    # using the parce here to not duplicate it 
    # parser will make sure that that body will only have one thing to pass which in this case Price
    parser = reqparse.RequestParser()
    parser.add_argument('price',
        type=float,
        required=True,
        help="This field can't left blank"
    )
    parser.add_argument('store_id',
        type=float,
        required=True,
        help="Every item needs a store id"
    )
    
    @jwt_required()
    def get(self, name):
        # this function won't return a list nor item it will return object for that we use next
        # we can use list(filter(lambda x: x['name'] == name, items)) but because this will always return one item we use next
        # we use next to give us the first item of the object filter 
        # None if there is not item instead of breaking the program

        # item = next(filter(lambda x: x['name'] == name, items), None)
        # return {'item': item}, 200 if item else 404
        items = ItemModule.find_by_name(name)
        if items:
            return items.json()
        return {'message': "item not found"}, 404
    
    def post(self, name):
        if ItemModule.find_by_name(name):
            return {'message': 'Item already existed.'}, 400

        data = Item.parser.parse_args()
        item = ItemModule(name, **data)
        try:
            item.save_to_db()
        except:
            return {'message': 'Wrongaaaaaaaaaaaaaa'}
        return item.json(), 201
        
        
        # if next(filter(lambda x: x['name'] == name, items), None) is not None:
        #     return {'items': f"An item with name {name} already exists."}, 400
        
        # item = {'name': name, 'price': data['price']}
        # items.append(item)
        # return item

    @jwt_required()
    def delete(self, name):
        # we are using the global variable to update it
        # if we don't use global it will consider local var 
        # Then we are keeping everything except the name mentioned in the API
        
        # global items
        # items = list(filter(lambda x: x['name'] != name, items))
        # return {'items': items}
        
        # item = ItemModule.find_by_name(name)
        # if item:
        #     connection = sqlite3.connect('data.db')
        #     cursor = connection.cursor()
        #     query = "DELETE FROM items WHERE name = ?"
        #     cursor.execute(query, (item.name,))
        #     connection.commit()
        #     connection.close()
        #     return {'message': f'Item {name} deleted successfully.'}
        # return {'message': f'Item {name} is not exist'}
        item = ItemModule.find_by_name(name)
        if item:
            item.delete_from_db()
        return {'message': 'Item has been deleted'}

    @jwt_required()
    def put(self, name):
        # we are using parser to make sure we only price will passed to update
        data = Item.parser.parse_args()
        item = ItemModule.find_by_name(name)
        
        if item is None:
            item = ItemModule(name,**data) # it will pass data['price'],data['store_id']
        else:
            item.price = data['price']
            
        return item.json()
            


class ItemList(Resource):
    def get(self):
        return {'items': [item.json() for item in ItemModule.query.all()]}