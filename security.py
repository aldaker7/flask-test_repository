from werkzeug.security import safe_str_cmp #for safe comparing
from models.user import UserModel

def authenticate(username, password):
    user = UserModel.find_by_username(username)
    if user and user.password == password:
        return user

# it's a method inside JWT flask
def identity(payload):
    userid = payload['identity']
    return UserModel.find_by_id(userid)