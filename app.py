from flask import Flask, request
from flask_restful import  Api
from flask_jwt import JWT
from security import authenticate, identity
from resources.user import UserRegister
from resources.items import Item, ItemList
from resources.stores import Store, StoreList
from db import db


app = Flask(__name__)

# my sql link
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
# flask track will be off for sqlalchemy but not sqlalchemy and we do that because it takes resources
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False
db.init_app(app)
# JWT to authenticate client to be able to use our API
app.secret_key = 'Ahmad'
api = Api(app)

# before anything below this method will run
@app.before_first_request
def create_tables():
    db.create_all()

jwt = JWT(app, authenticate, identity)

api.add_resource(Item, '/items/<string:name>') 
api.add_resource(ItemList, '/items') 
api.add_resource(UserRegister, '/register') 
api.add_resource(Store, '/store/<string:name>') 
api.add_resource(StoreList, '/stores') 


app.run(port=5000, debug=True)